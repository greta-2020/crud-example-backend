const router = require('express').Router()
const db = require('../db/db')

router.get('/', function(req, res) {
  db.query('SELECT * FROM paradigme')
  .then(results => res.json(results))
})

module.exports = router