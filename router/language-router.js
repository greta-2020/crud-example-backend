const router = require('express').Router()
const languagesController = require('../controllers/languages-controller')

router.get('/', languagesController.getAll)
router.get('/:id', languagesController.getOne)
router.put('/:id', languagesController.update)
router.post('/', languagesController.create)
router.delete('/:id', languagesController.destroy)

module.exports = router