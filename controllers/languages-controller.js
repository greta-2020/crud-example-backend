const db = require('../db/db')

const getId = (req) => +req.params.id
const getAttributes = (req) => {
  return {
    name: req.body.name,
    description: req.body.description
  }
}

const getAll = (req, res) => {
  db.query('SELECT * FROM language', [])
    .then(results => res.json(results))
    .catch(error => console.error(error))
}

const getOne = (req, res) => {
  const id = getId(req)
  console.log('id ? ', id)
  db.query('SELECT * FROM language WHERE id = ?', [id])
    .then(results => {
      if (results.length > 0) {
        res.json(results[0])
        return
      } else {
        res.status(404).json({error: 'language does not exist'})
      }
    })
}

const create = (req, res) => {
  const attributes = getAttributes(req)
  db.query(
    'INSERT INTO language (`name`) VALUES(?)',
    [attributes.name]
  )
    .then(results => db.query('SELECT * FROM language WHERE id = ?', [results.insertId]))
    .then(results => {
      console.log('new lange inséré : ', results[0])
      res.json(results[0])
    })
}

const update = (req, res) => {
  const id = getId(req)
  const attributes = getAttributes(req);
  db.query(
    'UPDATE language SET name = ?, description = ? WHERE id = ?',
    [attributes.name, attributes.description, id]
  )
  .then(result => db.query('SELECT * FROM language WHERE id = ?', [id]))
  .then(result => res.json(result))
}

const destroy = (req, res) => {
  const id = getId(req)
  db.query(
    'DELETE FROM language WHERE id = ?',
    [id]
  )
    .then(results => res.json({ message: `language ${id} supprimée` }))
    .catch(error => {
      console.error('erreur suppression language: ', error)
    })
}

module.exports = {
  getOne, getAll, update, create, destroy
}