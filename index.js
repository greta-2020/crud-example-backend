const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()
const languageRouter = require('./router/language-router')
const paradigmeRouter = require('./router/paradigme-router')

app.use(cors())
app.use(bodyParser.json())

app.set('view engine', 'pug');

app.use('/languages/', languageRouter)
app.use('/paradigmes/', paradigmeRouter)
app.get('/', function(req, res) {
  res.render('index')
})

const PORT = process.env.PORT || 3000

app.listen(PORT, function() {
  console.log('Server Crud-Languages start to listen on port ' + PORT + ' ... ')
});
