const mysql = require('mysql')
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'languages_app',
  password: 'secret',
  database: 'languages'
})

connection.connect()

const query = (sql, values) => {
  return new Promise((resolve, reject) => {
    console.log('SQL : ', sql)
    connection.query(sql, values, function (error, results) {
      if (error) reject(error);
      resolve(results)
    })
  })
}

// module.exports = { query: query }
module.exports = { query }